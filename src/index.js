import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import { Provider } from 'react-redux'
import configureStore from './REDUX/store/configureStore'
import registerServiceWorker from './registerServiceWorker';

import './index.css'

export const store = configureStore()

render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
registerServiceWorker();
