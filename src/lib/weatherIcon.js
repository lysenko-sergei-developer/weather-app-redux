export const weatherIcon = [
    {
        name: '01d',
        path: 'img/day/01d.png'
    },
    {
        name: '02d',
        path: 'img/day/02d.png'
    },
    {
        name: '03d',
        path: 'img/day/03d.png'
    },
    {
        name: '04d',
        path: 'img/day/04d.png'
    },
    {
        name: '09d',
        path: 'img/day/09d.png'
    },
    {
        name: '10d',
        path: 'img/day/10d.png'
    },
    {
        name: '11d',
        path: 'img/day/11d.png'
    },
    {
        name: '13d',
        path: 'img/day/13d.png'
    },
    {
        name: '50d',
        path: 'img/day/50d.png'
    },
    {
        name: '01n',
        path: 'img/night/01n.png'
    },
    {
        name: '02n',
        path: 'img/night/02n.png'
    },
    {
        name: '03n',
        path: 'img/night/03n.png'
    },
    {
        name: '04n',
        path: 'img/night/04n.png'
    },
    {
        name: '09n',
        path: 'img/night/09n.png'
    },
    {
        name: '10n',
        path: 'img/night/10n.png'
    },
    {
        name: '11n',
        path: 'img/night/11n.png'
    },
    {
        name: '13n',
        path: 'img/night/13n.png'
    },
    {
        name: '50n',
        path: 'img/night/50n.png'
    },
];

export default weatherIcon;