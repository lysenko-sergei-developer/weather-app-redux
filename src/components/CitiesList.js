import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

import weatherIcon from '../lib/weatherIcon'
import './CitiesList.css'

class CitiesList extends Component {
    costructor() {
        
        this.citiesObject;
        this.currentWeather;
    }

    uniqueID = () => {
        let text = "";
        let length = 8
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < length; i++)
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    handlerDeleteCity = (index) => {
        const {cities} = this.props.weather;
        const {deleteCityFromLocal} = this.props.Actions;
        
        const newCities = [
            ...cities.slice(0, index),
            ...cities.slice(index + 1)
        ];

        localStorage.removeItem("cities");
        localStorage.setItem("cities", JSON.stringify(newCities));

        deleteCityFromLocal(newCities);
    }

    render() {
      const {cities} = this.props.weather;

      const citiesList = Object.values(cities).map((city, i) => { 
        this.currentWeather = weatherIcon.filter((obj) => obj.name === city.list[0].weather[0].icon)

        return (
          <div className="history-container" key={this.uniqueID()}>
            <p className="history-city">{city.city.name}</p>
            <span className="history-temperature">{Math.floor(city.list[0].main.temp-273)} °C</span>
            <img className="weather-icon" src={this.currentWeather[0].path} alt={this.currentWeather[0].name} />
            <img className="close-icon" src="img/ui/close.svg" alt="close" onClick={() => this.handlerDeleteCity(i)}/>
          </div>
        )
      })
      
      return <div className="CitiesList">{citiesList}</div>;
    }
}


function mapStateToProps(state) {
    return {
        weather: state.weather,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CitiesList);
