import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

import CitiesList from './CitiesList'

import './Menu.css'

class Menu extends Component {
  constructor() {
    super()
    
    this.weatherInput = ''
    this.alreadySave = false;
  }

  handleSearchWeather = (e) => {
    const {searchWeather} = this.props.Actions
    searchWeather(this.weatherInput)
  }
  
  handleOnChangeWeather = (e) => {
    this.weatherInput = e.target.value
  }

  validateInput = (e) => {
    let string = e.target.value
    console.log(typeof string)
    if (Number.isInteger(string)) {
      console.log('all is ok')
      return true
    } else {
      console.error('input isnt string')
      return false
    }
  }

  getLastSearchCity = () => {
    const { lastSearch }  = this.props.weather
    const lastCityName = lastSearch.city.name
    
    return lastCityName
  }

  handlerSaveCity = (e) => {
    const {cities, lastSearch} = this.props.weather
    const {saveCityToLocal} = this.props.Actions
    const newCities = cities.concat(lastSearch)
    localStorage.setItem("cities", JSON.stringify(newCities))
    saveCityToLocal(newCities)

    this.alreadySave = true;
  }

  render() {
    const { searchSuccess, cities } = this.props.weather
    const cityName = searchSuccess && this.getLastSearchCity();

    return(
      <div className="Menu">
        <div className="search-container">
          <input 
            type="text" 
            placeholder="Input your city"
            onChange={this.handleOnChangeWeather}
            onSubmit={this.handleSearchWeather}
          />
          <button onClick={this.handleSearchWeather}>Search</button>
          { searchSuccess && !this.alreadySave ? 
            <div className="last-search-city">
              <p>Last your search {cityName}</p>
              <p>Would you like add this city to the set? <button onClick={this.handlerSaveCity}>yes</button></p>
            </div>
            :
            ''
          }
        </div>
        { (cities.length > 0) ?
          <div className="save-cities-container">
            <CitiesList />
          </div>
          :
          null
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        weather: state.weather,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
