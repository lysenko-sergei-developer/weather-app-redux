import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

import Menu from './Menu'
import WeatherData from './WeatherData'

import './App.css'

class App extends Component {

  componentWillMount() {
    const {getTrackedCity} = this.props.Actions;
    const cities = this.handlerGetTrackedCity()

    if (cities !== null) getTrackedCity(cities)
  }

  handlerGetTrackedCity = () =>  { 
    let cities = localStorage.getItem("cities") 
    
    if (cities !== 'undefined' && cities !== null) 
      return JSON.parse(cities)
    else 
      return []
  }

  render() {
    return(
      <div className="App">
        <Menu />
        <WeatherData />
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        weather: state.weather,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
