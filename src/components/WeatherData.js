import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

import WeatherDashboard from './WeatherDashboard'

import './WeatherData.css'

class WeatherData extends Component {

  componentDidMount = () => {
    this.handlerWatch()
  }

  handlerWatch = (e) => {
      function startTime(self) {
          var today = new Date();
          var d = today.toDateString();
          var h = today.getHours();
          var m = today.getMinutes();
          var s = today.getSeconds();
          m = checkTime(m);
          s = checkTime(s);
          watch.innerHTML = d + '|' + h + ":" + m + ":" + s;
          let t = setTimeout(startTime, 500);
      }

      function checkTime(i) {
          if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
          return i;
      }

      const watch = this.refs.watch
      startTime(watch)
  }


  render() {
    return(
      <div className="WeatherData">
        <div>Time now: <span ref='watch'></span></div>
        <WeatherDashboard />
        <button className="save-button">Save</button>
      </div>
    )
  }
}

function mapStateToProps(state) {
    return {
        weather: state.weather,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherData);
