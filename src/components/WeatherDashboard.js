import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as Actions from '../REDUX/actions/actions';

class WeatherDashboard extends Component {
    render() {
        const {lastSearch} = this.props.weather;
        console.log(lastSearch)
        if(lastSearch.length > 0 && lastSearch.list.length > 7) {
          const weatherData = [...lastSearch.list.slice(0, 7)]
    
          const weatherList = weatherData.forEach((i) => {
                return(
                  <div className="weather-chunk">
                    <img src={i.weather[0].icon} alt="weather-icon" className="weather-icon"/>
                    <p className="city-temperature">{i.main.temp - 273}</p>
                    <p className="weather-wind">{i.wind.speed}</p>
                  </div>
                )
              })
        }

        return (
            <div className="WeatherDashboard">
              <div className="weather-container">
                <weatherList />
              </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        weather: state.weather,
    }
}

function mapDispatchToProps(dispatch) {
    return {
        Actions: bindActionCreators(Actions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(WeatherDashboard);
