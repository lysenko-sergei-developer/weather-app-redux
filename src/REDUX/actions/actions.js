import {
  GET_TRACKED_CITY,
  SEARCH_CITY,
  SEARCH_CITY_SUCCESS,
  SEARCH_CITY_FAILURE,
  GET_DATA_FOR_CITY,
  SAVE_CITY_TO_LOCAL,
  DELETE_CITY_FROM_LOCAL
} from './constants'
import request from 'superagent'

export function getTrackedCity(cities) {
    return {
        type: GET_TRACKED_CITY,
        payload: cities
    }
}

function searchCity(value) {
    return {
        type: SEARCH_CITY,
        payload: value
    }
}

function searchCitySuccess() {
    return {
        type: SEARCH_CITY_SUCCESS,
        payload: true
    }
}

function searchCityFailure() {
    return {
        type: SEARCH_CITY_FAILURE,
        payload: false
    }
}

export function getDataForCity(value) {
    return {
        type: GET_DATA_FOR_CITY,
        payload: value
    }
}

export function saveCityToLocal(arr) {
    return {
        type: SAVE_CITY_TO_LOCAL,
        payload: arr
    }
}

export function deleteCityFromLocal(arr) {
    return {
        type: DELETE_CITY_FROM_LOCAL,
        payload: arr
    }
}


export function searchWeather (value) {
    const API = `http://api.openweathermap.org/data/2.5/forecast`;
    const API_KEY = `4b20b5aa915111d77497a2e65c9a9ebc`

    return dispatch => {    
        request
            .get(`${API}`)
            .query({q: `${value}`})
            .query({appid: `${API_KEY}`})
            .end((err, res) => {
                if (err || !res.ok) {
                    dispatch(searchCityFailure(err))
                } else {
                    dispatch(searchCity(JSON.parse(res.text)))
                    dispatch(searchCitySuccess())
                }
            })
    }       
    
  }