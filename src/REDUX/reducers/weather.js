import {
  GET_TRACKED_CITY,
  SEARCH_CITY,
  SEARCH_CITY_SUCCESS,
  SEARCH_CITY_FAILURE,
  GET_DATA_FOR_CITY,
  SAVE_CITY_TO_LOCAL,
  DELETE_CITY_FROM_LOCAL
} from '../actions/constants'

const initialState = {
    cities: [],
    lastSearch: {},
    searchSuccess: false,
};

export default function main(state = initialState, action) {
  switch(action.type) {
    case GET_TRACKED_CITY:
      return { ...state, cities: action.payload };
    
    case SEARCH_CITY:
      return { ...state, lastSearch: action.payload};

    case SEARCH_CITY_SUCCESS:
      return { ...state, searchSuccess: action.payload};
    
    case SEARCH_CITY_FAILURE:
      return { ...state, searchSuccess: action.payload};

    case GET_DATA_FOR_CITY:
      return { ...state, money: action.payload};

    case SAVE_CITY_TO_LOCAL:
      return { ...state, cities: action.payload};
    
    case DELETE_CITY_FROM_LOCAL:
      return { ...state, cities: action.payload};
    
    default:
      return state;
  }
}